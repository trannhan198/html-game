import React from 'react'
import style from './style.module.scss'

const index = () => {
    return (
        <div id="mySidebar" className={style.sidebar}>
            <a href="javascript:void(0)" className={style.closebtn} onclick="closeNav()">×</a>
            <div className="form_search">
                <form action>
                    <div className="search">
                        <input type="text" placeholder="Bạn sẽ chơi gì hôm nay?" />
                        <svg className="fill_svg_v2" viewBox="0 0 512 512" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em"><path fillRule="evenodd" clipRule="evenodd" d="M208 48c-88.366 0-160 71.634-160 160s71.634 160 160 160 160-71.634 160-160S296.366 48 208 48zM0 208C0 93.125 93.125 0 208 0s208 93.125 208 208c0 48.741-16.765 93.566-44.843 129.024l133.826 134.018c9.366 9.379 9.355 24.575-.025 33.941-9.379 9.366-24.575 9.355-33.941-.025L337.238 370.987C301.747 399.167 256.839 416 208 416 93.125 416 0 322.875 0 208z" /></svg>
                    </div>
                </form>
            </div>
            <div className="list_g">
                <div className="list_hot">
                    <h4>Phổ biến trong tuần</h4>
                    <div className="list">
                        <div className="list_items">
                            <img src="./img/smileyworld-bubble-shooter.jpg" alt="" />
                            <div className="title_item"><p>smileyworld</p></div>
                        </div>
                        <div className="list_items">
                            <img src="./img/assassins-creed-freerunners.jpg" alt="" />
                            <div className="title_item"><p>smileyworld</p></div>
                        </div>
                        <div className="list_items">
                            <img src="./img/battles-of-seas.jpeg" alt="" />
                            <div className="title_item"><p>smileyworld</p></div>
                        </div>
                        <div className="list_items">
                            <img src="./img/Garden-Tales-3.jpeg" alt="" />
                            <div className="title_item"><p>smileyworld</p></div>
                        </div>
                    </div>
                </div>
                <div className="list_hot">
                    <h4>Game bạn đã chơi gần đây</h4>
                    <div className="list">
                        <div className="list_items">
                            <img src="./img/idle-survival.jpeg" alt="" />
                            <div className="title_item"><p>smileyworld</p></div>
                            <div className="title_item"><p>smileyworld</p></div>
                        </div>
                        <div className="list_items">
                            <img src="./img/Mr-Noobs-vs-Stickman.jpeg" alt="" />
                            <div className="title_item"><p>smileyworld</p></div>
                        </div>
                        <div className="list_items">
                            <img src="./img/smileyworld-bubble-shooter.jpg" alt="" />
                            <div className="title_item"><p>smileyworld</p></div>
                        </div>
                        <div className="list_items">
                            <img src="./img/smileyworld-bubble-shooter.jpg" alt="" />
                            <div className="title_item"><p>smileyworld</p></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default index