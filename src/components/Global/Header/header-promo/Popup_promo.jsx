import React from 'react'
import style from './style.module.scss'

const Popup_promo = () => {
    return (
        <div className={style.popup_promo}>
            <p className='title_gb'>GIAO HÀNG MIỄN PHÍ CHO THÀNH VIÊN</p>
            <p>Đăng ký thành viên adiClub để hưởng thụ dịch vụ giao hàng miễn phí! Hoặc bạn chỉ được nhận ưu đãi miễn phí giao hàng với hóa đơn có trị giá ít nhất 1.6 triệu đồng</p>
            <p>GIAO HÀNG MIỄN PHÍ CHO THÀNH VIÊN CỦA ADICLUB</p>
        </div>
    )
}

export default Popup_promo