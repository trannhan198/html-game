import React, { useEffect, useState } from 'react'
import Popup_promo from './Popup_promo'
import style from './style.module.scss'

const index = () => {

    let [titles, setTitle] = useState(true)
    let [togglePopup, setTogglePopup] = useState(false)

    useEffect(() => {
        let myInterval = setInterval(() => {
            setTitle(!titles)
        }, 4000)

        return function cleanup() {
            clearInterval(myInterval)
        }
    }, [titles])

    let handleOpenPopup = ()=>{
        setTogglePopup(!togglePopup)
    }

    return (
        <div className={style.body_promo}>
            <div className={style.wrap_header_promo}>
                <div className={style.title_promo}>
                    <p onClick={handleOpenPopup}>
                        {
                            titles ? ' Trả hàng dễ dàng ' : 'Giao hàng miễn phí cho các thành viên'
                        } <button className={style.button_popup_promo}><i class="fas fa-chevron-down"></i></button>
                    </p>
                </div>
            </div>

            {
                togglePopup ? <Popup_promo></Popup_promo> : ''
            }

        </div>
    )
}

export default index