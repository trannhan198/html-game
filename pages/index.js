import Image from 'next/image'
import styles from '../styles/Home.module.css'
import Homes from '../src/components'

export default function Home() {
	return (
		<div>
		   <Homes></Homes>
		</div>
	)
}
