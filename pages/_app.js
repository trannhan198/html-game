import '../styles/globals.css'
import Fotter from '../src/components/Global/Footer'
import Template from '../src/Template'
import style from './style.module.scss'
import Header from '../src/components/Global/Header'

function MyApp({ Component, pageProps }) {
    return <div className={style.body}>
        <Template>
            <Header></Header>

            <Component {...pageProps} />

            <Fotter></Fotter>
        </Template>
    </div>
}

export default MyApp
